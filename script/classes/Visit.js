/*
При успешном добавлении карточки в базу, в качестве ответа вы получите переданный вами объект с добавлением id
карточки в базе.
Пример ответа:
	{
   		id: 16,
        patientFullName: 'Ron Swanson',
        doctorType: 'cardiologist',
        visitPurpose: 'some purpose',
        visitDescription: 'some description',
        urgency: 'regular, priority, emergency',
        bloodPressure: '120/70',
        bodyMassIndex: '20.1',
        transferredDiseases: 'none',
        patientAge: '54'
	}

	{
   		id: 16,
        patientFullName: 'John Johnson',
        doctorType: 'dentist',
        visitPurpose: 'some purpose',
        visitDescription: 'some description',
        urgency: 'regular, priority, emergency',
        dateOfLastVisit: '26.07.2018'

	}

{
    	id: 16,
        patientFullName: 'Emma Watson',
        doctorType: 'therapist',
        visitPurpose: 'some purpose',
        visitDescription: 'some description',
        patientAge: '54'
}
 */


import {ModalCreateCard} from "./ModalCreateCard.js";

class Visit {
    constructor(dataObj) {
        this.dataObj = dataObj;
        this.id = dataObj.id;
        this.elem = null; //card main container
        this.deleteButton = null;
        this.extendedOptionContainer = null;
        this.editButton = null;
        this.flagContainer = null;
    }

    render() {
        const {urgency, patientFullName, doctorType} = this.dataObj;
        this.elem = document.createElement('div');
        this.elem.className = 'card__content-container';
        this.elem.id = `card${this.id}`;
        this.elem.setAttribute('data-id', `${this.id}`);
        this.elem.setAttribute('draggable', `true`);
        this.elem.setAttribute('data-drop', `true`);

        //create flag container
        this.flagContainer = document.createElement('div');
        this.flagContainer.className = 'card__flag';
        if (urgency === 'regular') {
            this.flagContainer.classList.add('card__urgency--regular');
        }

        if (urgency === 'priority') {
            this.flagContainer.classList.add('card__urgency--priority');
        }

        if (urgency === 'emergency') {
            this.flagContainer.classList.add('card__urgency--emergency');
        }

        this.elem.append(this.flagContainer);

        //create edit button and add event listener
        this.editButton = document.createElement('div');
        this.editButton.className = 'card__change';
        this.editButton.addEventListener('click', this.editCard.bind(this));
        this.editButton.setAttribute('data-drop', `false`);


        //create delete button and add event listener
        this.deleteButton = document.createElement('div');
        this.deleteButton.className = 'card__del';
        this.deleteButton.addEventListener('click', this.deleteCard.bind(this));
        this.deleteButton.setAttribute('data-drop', `false`);


        //create button's container and append buttons
        const buttonContainer = document.createElement('div');
        buttonContainer.className = 'card__ico-container';
        buttonContainer.append(this.editButton);
        buttonContainer.append(this.deleteButton);
        buttonContainer.setAttribute('data-drop', `false`);


        //append button container to main container and create fields
        this.elem.append(buttonContainer);
        this.elem.insertAdjacentHTML('beforeend', `
        <div class="card__content" data-drop="false">
            <span class="card__content-first" data-drop="false">Patient Name:</span>
            <span class="card__content-second" data-drop="false">${patientFullName.toUpperCase()}</span>
        </div>

        <div class="card__content" data-drop="false">
            <span class="card__content-first" data-drop="false">Doctor:</span>
            <span class="card__content-second" data-drop="false">${doctorType.toUpperCase()}</span>
        </div>
        
        `);

        ////create show more button and add listeners
        const showDetailsButton = document.createElement('span');
        showDetailsButton.className = 'show-more';
        showDetailsButton.innerText = 'SHOW DETAILS';
        showDetailsButton.addEventListener('click', this.showDetails.bind(this));
        showDetailsButton.setAttribute('data-drop', `false`);

        ////create extended option container
        this.extendedOptionContainer = document.createElement('div');
        this.extendedOptionContainer.className = 'card__extended-options';
        this.extendedOptionContainer.setAttribute('data-drop', `false`);

        this.elem.append(showDetailsButton);
        this.elem.append(this.extendedOptionContainer);

        return this.elem;
    }

    /**
     * @description call edit modal window, collect information, send request
     */
    editCard() {
        const editModal = new ModalCreateCard('edit-card', 'edit-card', 'SAVE CHANGES');
        const editModalForm = editModal.render();

        document.body.append(editModalForm);

        const editModalSelect = editModalForm.querySelector('[name="doctorType"]');
        editModalSelect.value = this.dataObj.doctorType;
        console.log(editModalSelect.value);

        editModal.chooseSelect({
            target: editModalSelect
        });


        Object.keys(this.dataObj).forEach(key => {
            if(key !== 'id'){
            editModalForm.querySelector(`[name="${key}"]`).value = this.dataObj[key];
            }
        }) ;


        document.getElementById('edit-card').addEventListener('submit', this.deleteCard.bind(this));
    }

    /**
     * @description call modal window for password, send DELETE request and remove card
     */
   async deleteCard() {
        console.log('delete');
        const config = {
            headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
        };
        const {data} = await axios.delete(`http://cards.danit.com.ua/cards/${this.id}`, config);
        if (data.status === 'Success') {
            // alert(data.status);
            this.elem.remove();
        } else if (data.status === 'Error') {
            alert(data.masssage);
        }

    }

    /**
     * @description change class of show details elements
     */
    showDetails() {
        this.extendedOptionContainer.classList.toggle('card__extended-options--active');
        this.elem.classList.toggle('card__content-container--active');

    }

}


export {Visit};
