/**
 * @description get token from local storage, sent request to http://cards.danit.com.ua/cards, return response.data
 *
 *response is like:
 * {
    "patientFullName": "Ron Swanson",
    "doctorType": "cardiologist",
    "visitPurpose": "some purpose",
    "visitDescription": "some description",
    "urgency": "emergency",
    "bloodPressure": "120/70",
    "bodyMassIndex": "20.1",
    "transferredDiseases": "none",
    "patientAge": "54",
    "id": "7794"
}
 *
 *
 * @param body {object} - data from modal "create visit"
 *
 *body is like:
 *
 *{
        patientFullName: 'Ron Swanson',
        doctorType: 'cardiologist',
        visitPurpose: 'some purpose',
        visitDescription: 'some description',
        urgency: 'emergency',
        bloodPressure: '120/70',
        bodyMassIndex: '20.1',
        transferredDiseases: 'none',
        patientAge: '54'
    }
 *
 * @returns {object}
 */


async function postVisitCard(body) {
	const config = {
		headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
	};
    const {data} = await axios.post('http://cards.danit.com.ua/cards', body, config);
    return data;
}

export {postVisitCard};