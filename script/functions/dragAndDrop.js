const dragAndDrop = {
    mainContainer: document.getElementById('card__main-container'),
    dragElement: null,

    addEventListeners(){
        this.mainContainer.addEventListener('dragstart', this.dragStart.bind(this));
        this.mainContainer.addEventListener('drop', this.dragDrop.bind(this));
        this.mainContainer.addEventListener('dragover', this.dragOver.bind(this));

    },

    dragStart(event) {
        this.dragElement = event.target;
        console.log(this.dragElement);
    },

    dragDrop(event) {
        console.log('drop');
        console.log(event.target);
        event.preventDefault();
        const cardId = event.currentTarget.getAttribute('id');
        if (event.target === event.currentTarget) {
            console.log('(event.target === event.currentTarget)');
            console.log(event.target);
            console.log(event.currentTarget);
            event.currentTarget.append(this.dragElement);
        } else {
            event.target.closest('.card__content-container').before(this.dragElement);
        }

        this.dragElement = null;
    },

    dragOver(event) {
        console.log('drag over');
        event.preventDefault();
    },

    init(){
        this.addEventListeners();
    }
};



export {dragAndDrop}